class Image < ApplicationRecord
  belongs_to :category
  has_one_attached :main_image

  validate :acceptable_image

  before_save :update_image_attributes

  def acceptable_image
    unless main_image.attached?
      unless main_image.byte_size <= 1.megabyte
        errors.add(:main_image, "is too big")
      end
    end
    acceptable_types = ["image/jpeg", "image/png"]
    unless acceptable_types.include?(main_image.content_type)
      errors.add(:main_image, "must be a JPEG or PNG")
    end
  end

  private

  def update_image_attributes
    if main_image.present? && main_image.changed?
      self.image_content_type = main_image.content_type
      self.image_file_size = main_image.byte_size
    end
  end
end
