class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :set_images, only: [:show, :edit, :update, :destroy]

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    if @category.save
      flash[:notice] = "Category Created"
      redirect_to root_path
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @category.update(category_params)
      flash[:notice] = "Category Updated"
      redirect_to category_path
    else
      render 'edit'
    end
  end

  def destroy
  end

  private
  def set_category
    @category = Category.find_by id: params[:id]
  end

  def set_images
    @images = Image.where(category_id: params[:id])
  end

  def category_params
    params.require(:category).permit(:name)
  end
end
