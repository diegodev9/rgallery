class ImagesController < ApplicationController
  before_action :set_image, only: [:edit, :update, :destroy]

  def index
    @images = Image.all
    @active = 'active'
  end

  def new
    @image = Image.new
  end

  def create
    @image = Image.new(image_params)

    if @image.save
      flash[:notice] = "Image saved"
      redirect_to root_path
    else
      render :new
    end
  end

  def edit

  end

  def update
    if @image.update(image_params)
      flash[:notice] = "Image Updated"
      redirect_to category_path(@image.category_id)
    else
      render 'edit'
    end
  end

  def destroy
    category_id = @image.category_id
    @image.main_image.purge
    @image.destroy
    flash[:notice] = "Image Deleted"
    redirect_to category_path(category_id)
  end

  private

  def set_image
    @image = Image.find_by id: params[:id]
  end

  def image_params
    params.require(:image).permit(:image, :category_id, :image_title, :image_description, :image_file_size, :image_content_type, :remote_image_url, :main_image)
  end
end
