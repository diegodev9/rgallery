Rails.application.routes.draw do

  root 'images#index'
  get 'upload', to: 'images#new', as: 'upload'
  resources :images
  resources :categories, except: [:index]

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
